# 租车小程序前端

#### 项目介绍
用于租车公司预约的小程序

#### 软件架构
软件架构说明

#### 更新日志
 _2018-6-21 加入核销员管理订单_ 

 _2016-6-23 车辆列表中，可查看每辆车的价格日历表_ 

#### 安装教程

 **小程序端界面** 

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221631_9912ff1d_1974020.jpeg "1.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221643_fdd76cd2_1974020.jpeg "2.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0623/094131_30ffd83a_1974020.jpeg "10.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221653_8bb4aa51_1974020.jpeg "3.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221700_01b3f146_1974020.jpeg "4.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221708_78a19e55_1974020.jpeg "5.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221715_3a403e14_1974020.jpeg "6.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221726_dd39dc4d_1974020.jpeg "9.jpg")

 **核销员管理**

![输入图片说明](https://gitee.com/uploads/images/2018/0623/094039_953e4997_1974020.jpeg "11.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0623/094051_86056d90_1974020.jpeg "12.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0623/094058_14c58fa7_1974020.jpeg "13.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0623/094104_6317853e_1974020.jpeg "14.jpg")

 **后端界面** 

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221815_ad968105_1974020.png "1.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221823_cd89451e_1974020.png "2.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221831_928d763f_1974020.png "3.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221840_aea930db_1974020.png "4.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221847_8ef8a8d4_1974020.png "5.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221854_f09561ad_1974020.png "6.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221901_e0539eeb_1974020.png "7.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221907_15b4cff7_1974020.png "8.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221914_b6cb60b0_1974020.png "9.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221920_478681d6_1974020.png "10.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221927_b3d30925_1974020.png "11.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221933_90d7f90d_1974020.png "12.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221941_199cdf03_1974020.png "13.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221947_335e5224_1974020.png "14.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/221955_4965c12a_1974020.png "15.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/222002_7252f46a_1974020.png "16.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/222009_8cc50ab7_1974020.png "17.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/222016_eb726635_1974020.png "18.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/222023_d9326cd5_1974020.png "19.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/222030_7fcf7abc_1974020.png "20.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0609/222037_c294cd01_1974020.png "21.png")

#### 使用说明

 _注：各位同学，后端源码有偿提供，请尊重我的劳动成果，谢谢！淘宝购买地址：https://item.taobao.com/item.htm?spm=a1z10.5-c.w4002-2434671115.35.212d62bfgWRKao&id=571520511177(数量请拍10个，总额300元，请在留言备注你的邮箱，我会把后端发送给你，包后续所有更新)_ 

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


